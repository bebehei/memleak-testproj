PKG_CONFIG ?= pkg-config
pkg_config_packs := gdk-pixbuf-2.0

.PHONY: all valgrind clean
all: test valgrind
clean:
	rm -rf test icons.txt

test: test.c
	gcc -o $@ $(shell $(PKG_CONFIG) --libs --cflags ${pkg_config_packs}) $^

icons.txt:
	find /usr/share/icons -type f -name '*.svg' | head > icons.txt

valgrind: test icons.txt
	valgrind \
		--leak-check=full \
		--show-leak-kinds=definite \
		--errors-for-leak-kinds=definite \
		--num-callers=40 \
		--error-exitcode=123 \
		./test
