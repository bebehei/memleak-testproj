#include <gdk-pixbuf/gdk-pixbuf.h>

int main(int argc, char *argv[])
{
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	// icons.txt will get generated via Makefile
	// and will contain 10 svg icons found in /usr/share/icons
	fp = fopen("icons.txt", "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);

	while ((read = getline(&line, &len, fp)) != -1) {
		gchar *chomped = g_strchomp(g_strdup(line));
		GError *e = NULL;
		GdkPixbuf *pb = gdk_pixbuf_new_from_file(chomped, &e);

		if (e)  g_error_free(e);
		if (pb) g_object_unref(pb);
		g_free(chomped);
	}

	free(line);
	fclose(fp);
	return 0;
}
